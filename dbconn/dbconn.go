package dbconn

import (
	"encoding/json"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/boltdb/bolt"
	"github.com/bwmarrin/discordgo"
)

type config struct {
	Database databaseSettings
}

type databaseSettings struct {
	Directory   string
	Name        string
	Permissions int
}

// SimplifiedMessage represents a simplified
// version of discordgo.Message, keeping the
// most pertinent information to keep DB
// size as low as possible.
type SimplifiedMessage struct {
	ID          string
	ChannelID   string
	Content     string
	Timestamp   string
	Author      SimplifiedAuthor
	Attachments []SimplifiedAttachment
}

// SimplifiedAuthor is used in tandem with
// SimplifiedMessage, representing a
// slimmed-down version of discordgo.Author.
type SimplifiedAuthor struct {
	ID       string
	Username string
}

// SimplifiedAttachment is used in tandem
// with SimplifiedMessage, representing a
// slimmed-down version of
// discordgo.MessageAttachment.
type SimplifiedAttachment struct {
	ID  string
	URL string
}

var (
	connSettings config

	// Database is the reference to database connection
	Database *bolt.DB
)

func init() {
	if _, err := toml.DecodeFile("config.toml", &connSettings); err != nil {
		log.WithError(err).Fatal("Failed to load database settings")
	}

	if _, err := os.Stat(connSettings.Database.Directory); os.IsNotExist(err) {
		os.Mkdir(connSettings.Database.Directory, 0644)
	}
}

// CreateConnection creates the database connection for the program
func CreateConnection() {
	db, err := bolt.Open(connSettings.Database.Directory+"/"+connSettings.Database.Name, 0644, &bolt.Options{Timeout: 1 * time.Second})
	Database = db

	if err != nil {
		log.WithError(err).Fatal("Failed to connect to database")
	}

	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("messages"))
		return err
	})
}

// AddMessage adds a message to the database
// m: message to add
func AddMessage(m *discordgo.Message) error {
	err := Database.Update(func(tx *bolt.Tx) error {
		// Create simplified message based on message to store in DB
		simpleMessage := SimplifiedMessage{
			ID:        m.ID,
			ChannelID: m.ChannelID,
			Content:   m.Content,
			Timestamp: string(m.Timestamp),
			Author: SimplifiedAuthor{
				ID:       m.Author.ID,
				Username: m.Author.Username,
			},
		}

		// Add simplified attachments to simplified message
		for _, a := range m.Attachments {
			simpleAttachment := SimplifiedAttachment{
				ID:  a.ID,
				URL: a.URL,
			}
			simpleMessage.Attachments = append(simpleMessage.Attachments, simpleAttachment)
		}

		b := tx.Bucket([]byte("messages"))
		id := m.ID

		bytes, encerr := json.Marshal(simpleMessage)
		if encerr != nil {
			log.WithError(encerr).Error("Failed to encode message")
			return encerr
		}

		return b.Put([]byte(id), bytes)
	})

	if err != nil {
		log.WithError(err).Error("Failed to update DB")
		return err
	}

	return nil
}

// GetMessages returns the latest messages in the database, newest to oldest.
// amount: number of messages to return
func GetMessages(amount int) []SimplifiedMessage {
	var messages []SimplifiedMessage

	Database.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("messages"))
		c := b.Cursor()

		left := amount

		for k, v := c.Last(); k != nil && left >= 1; k, v = c.Prev() {
			var message SimplifiedMessage
			err := json.Unmarshal(v, &message)
			if err != nil {
				log.WithError(err).Error("Failed to unmarshal messages")
			}
			messages = append(messages, message)
			left--
		}

		return nil
	})

	return messages
}

// GetMessageByID returns a message by its Discord ID.
// id: ID of message to get
func GetMessageByID(id string) (SimplifiedMessage, error) {
	var message SimplifiedMessage

	err := Database.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("messages"))
		v := b.Get([]byte(id))

		err := json.Unmarshal(v, &message)
		if err != nil {
			return err
		}

		return nil
	})

	return message, err
}
