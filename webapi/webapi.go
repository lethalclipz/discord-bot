package webapi

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/lethalclipz/discord-bot/dbconn"
)

var (
	engine *gin.Engine
)

// StartGinServer starts the Gin server and sets up endpoint bindings.
func StartGinServer() {
	engine = gin.Default()

	engine.GET("/messages", getMessages)
	engine.GET("/messages/:id", getMessageByID)

	engine.Run()
}

func getMessages(c *gin.Context) {
	messages := dbconn.GetMessages(100)

	c.JSON(200, messages)
}

func getMessageByID(c *gin.Context) {
	id := c.Param("id")
	message, err := dbconn.GetMessageByID(id)
	if err != nil {
		c.JSON(404, gin.H{
			"message": "Message " + id + " not found",
		})
		return
	}

	c.JSON(200, message)
}
