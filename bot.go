package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/lethalclipz/discord-bot/dbconn"
	"gitlab.com/lethalclipz/discord-bot/msghandler"
	"gitlab.com/lethalclipz/discord-bot/webapi"

	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	"github.com/mattn/go-colorable"
)

type config struct {
	Account accountConfig
	Servers []string
}

type accountConfig struct {
	Token string
}

func init() {
	log.SetFormatter(&log.TextFormatter{ForceColors: true})
	log.SetOutput(colorable.NewColorableStdout())
}

func main() {
	dbconn.CreateConnection()
	go webapi.StartGinServer()

	var config config

	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		fmt.Println(err)
	}

	log.WithFields(log.Fields{
		"api_key": config.Account.Token,
	}).Info("Attempting to authenticate with Discord")
	discord, err := discordgo.New("Bot " + config.Account.Token)
	if err != nil {
		log.WithError(err).Error("Failed to authenticate with Discord.")
	}
	log.Info("Successfully logged into Discord.")

	discord.AddHandler(msghandler.HandleMessage)

	// Open the websocket and begin listening.
	if err = discord.Open(); err != nil {
		log.WithError(err).Error("Failed to open Discord websocket")
	}

	// Wait here until CTRL-C or other term signal is received.
	log.Info("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	log.Info("CTRL-C invoked, closing Discord session")

	// Cleanly close down the Discord session.
	discord.Close()

	log.Info("Discord session closed.")
}
