package msghandler

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/lethalclipz/discord-bot/dbconn"

	"bytes"
	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/bwmarrin/discordgo"
	qrcode "github.com/skip2/go-qrcode"
	"path/filepath"
)

type commandInfo struct {
	Prefix string
}

type messageHandlerFunc func(*discordgo.Session, *discordgo.MessageCreate) string

type commandMap map[string]command

type command struct {
	function messageHandlerFunc
	tooltip  string
}

var (
	functionMap   commandMap
	commandPrefix string
)

func init() {
	var commandConfig commandInfo

	fileString, _ := filepath.Abs("config.toml")

	if _, err := toml.DecodeFile(fileString, &commandConfig); err != nil {
		log.WithError(err).Error("Failed to read command prefix in config")
	}

	if commandConfig.Prefix == "" {
		log.Fatal("The command prefix must not be null.")
	}

	commandPrefix = commandConfig.Prefix
}

// HandleMessage handles messages, including parsing commands and logging
func HandleMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	var command, commandTrimmed string
	fields := strings.Fields(m.Content)
	if len(fields) > 0 {
		command = fields[0]
		commandTrimmed = strings.TrimLeft(command, commandPrefix)
	}

	if strings.HasPrefix(command, commandPrefix) {
		commandStruct, ok := functionMap[commandTrimmed]
		if ok {
			log.WithFields(log.Fields{
				"author":    m.Author.Username,
				"content":   m.Content,
				"timestamp": m.Timestamp,
			}).Info("Command received")
			message := commandStruct.function(s, m)
			if message != "" {
				s.ChannelMessageSend(m.ChannelID, message)
			}
		} else {
			s.ChannelMessageSend(m.ChannelID, "Command \""+commandPrefix+commandTrimmed+"\" not found. Type !help for info on available commands.")
		}
	}

	dbconn.AddMessage(m.Message)
}

func init() {
	functionMap = make(commandMap)
	functionMap.addFunctionsToMap()
}

func (m *commandMap) addFunctionsToMap() {
	functionMap["help"] = command{help, "Lists all available commands."}
	functionMap["purge"] = command{purge, "Deletes previous <param> messages in current channel (max 99)."}
	functionMap["setstatus"] = command{setStatus, "Sets the bot's status."}
	functionMap["type"] = command{startTyping, "Starts typing!"}
	functionMap["embedtest"] = command{embedTest, "Sends a sample embed message."}
	functionMap["delayedmessage"] = command{delayedMessage, "Sends a message after 5 seconds."}
	functionMap["qr"] = command{sendQRCode, "Sends a QR with the given message."}
	functionMap["pin"] = command{pin, "Pins the message with the command."}
}

func help(s *discordgo.Session, m *discordgo.MessageCreate) string {
	embed := discordgo.MessageEmbed{
		Description: "A list of commands available.",
		Timestamp:   time.Now().Format(time.RFC3339),
		Title:       "The Bestestest Help",
	}

	for k, v := range functionMap {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  commandPrefix + k,
			Value: v.tooltip,
		})
	}

	_, err := s.ChannelMessageSendEmbed(m.ChannelID, &embed)
	if err != nil {
		log.WithError(err).Warn("Sending help message failed")
	}

	return ""
}

func purge(s *discordgo.Session, m *discordgo.MessageCreate) string {
	var amountToDelete int
	var err error
	fields := strings.Fields(m.Content)
	if len(fields) >= 2 {
		amountToDelete, err = strconv.Atoi(fields[1])
		if err != nil {
			return "Error: expected integer value"
		}

		if amountToDelete < 0 || amountToDelete > 99 {
			return "Error: value must be integer from 0–99"
		}

		amountToDelete++
	} else {
		amountToDelete = 100
	}

	messages, _ := s.ChannelMessages(m.ChannelID, amountToDelete, "", "", "")
	var messageIDs []string
	for _, message := range messages {
		messageIDs = append(messageIDs, message.ID)
	}
	err = s.ChannelMessagesBulkDelete(m.ChannelID, messageIDs)
	if err != nil {
		log.WithError(err).Warn("Failed to purge messages")
	}

	return ""
}

func setStatus(s *discordgo.Session, m *discordgo.MessageCreate) string {
	statusFields := strings.Fields(m.Content)[1:]
	status := ""

	for _, field := range statusFields {
		status += field + " "
	}
	statusTrimmed := strings.Trim(status, " ")

	err := s.UpdateStatus(0, statusTrimmed)
	if err != nil {
		return "Failed to update status!"
	}

	if statusTrimmed != "" {
		return "Now playing: **" + statusTrimmed + "**"
	}

	return ""
}

func startTyping(s *discordgo.Session, m *discordgo.MessageCreate) string {
	s.ChannelTyping(m.ChannelID)
	guild, err := s.GuildCreate("NewGuild")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Successfully created guild: " + guild.Name)
	}
	return ""
}

func embedTest(s *discordgo.Session, m *discordgo.MessageCreate) string {
	embed := discordgo.MessageEmbed{
		Author: &discordgo.MessageEmbedAuthor{
			Name: "Carson",
		},
		Color:       0x72cbff,
		Description: "This is my face",
		Fields: []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:   "Non-inline field",
				Value:  "Hi!",
				Inline: false,
			},
			&discordgo.MessageEmbedField{
				Name:   "Inline field",
				Value:  "Hello!",
				Inline: true,
			},
		},
		Image: &discordgo.MessageEmbedImage{
			URL: "http://i.imgur.com/FRuaz2q.jpg",
		},
		Timestamp: time.Now().Format(time.RFC3339),
		Title:     "I am the best embed ever.",
	}

	_, err := s.ChannelMessageSendEmbed(m.ChannelID, &embed)
	if err != nil {
		fmt.Println(err)
	}

	return ""
}

func delayedMessage(s *discordgo.Session, m *discordgo.MessageCreate) string {
	s.ChannelTyping(m.ChannelID)

	timer := time.NewTimer(time.Second * 5)
	<-timer.C

	return "Finally sent message!"
}

func sendQRCode(s *discordgo.Session, m *discordgo.MessageCreate) string {
	fields := strings.Fields(m.Content)
	if len(fields) < 2 {
		return m.Author.Mention() + " !qr must be used with a message."
	}

	stringToEncode := m.Content[4:]

	s.ChannelTyping(m.ChannelID)

	png, err := qrcode.Encode(stringToEncode, qrcode.Low, -4)

	if err != nil {
		return m.Author.Mention() + " error encoding QR code."
	}

	var message *discordgo.Message
	message, err = s.ChannelFileSend(m.ChannelID, "QR-"+m.Author.Username+"-"+time.Now().Format(time.RFC3339)+".png", bytes.NewReader(png))

	if err != nil {
		log.WithError(err).Warn("QR message failed to send")
		return ""
	}

	log.WithFields(log.Fields{
		"content": stringToEncode,
		"url":     message.Attachments[0].ProxyURL,
	}).Info("Successfully sent QR code")

	return ""
}

func pin(s *discordgo.Session, m *discordgo.MessageCreate) string {
	err := s.ChannelMessagePin(m.ChannelID, m.ID)

	if err != nil {
		log.WithError(err).Warn("Failed to pin message")
	}

	return ""
}
